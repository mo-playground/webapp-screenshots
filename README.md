# webapp-screenshots

## Overview

This project is a self-contained solution for basic web application attack-surface monitoring using GitLab CI/CD, GitLab Pages, and a handful of free and open source security tools.

In a nutshell, it can do the following:

- Identify web services on a list of addresses you own
- Capture screenshots of these web services
- Build an authenticated web portal for you to visually see each site that was discovered

You can read more about it [in this blog](https://about.gitlab.com/blog/2023/01/11/monitor-web-attack-surface-with-gitlab/).

## Usage

You can clone or fork this project into your own space. You may wish to make your own copy private.

You should then follow the steps in the blog linked above to:

- Define your targets
- Schedule a pipeline
- Enable email notifications
- View your GitLab Pages site

## Contributing

Please open an issue with any questions, and feel free to submit an MR with fixes or improvements. You may want to check with us first before any major contributions, as we want this project to be a simple example without too much complexity.

Thanks!
