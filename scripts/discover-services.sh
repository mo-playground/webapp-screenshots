#!/bin/bash

# You may wish to dynamically generate a target file with each run.
# For this demo, we are using a list defined inside project CI variables.
TARGETS=$TARGET_FILE

# create output directory
mkdir ./targets

# Identify web services
echo "Identifying web services across $(cat "$TARGETS" | wc -l) targets..."
cat "$TARGETS" | bin/httpx -o targets/web-services.txt -p 80,443

echo "Discovered $(cat targets/web-services.txt | wc -l) web services."
